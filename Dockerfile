FROM circleci/node:10.14-browsers
WORKDIR /app
COPY package.json /app
COPY . /app