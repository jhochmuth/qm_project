import {Selector} from 'testcafe';
const assert = require('assert')

fixture('problemOne').page('https://www.quantummetric.com/integrations/adobe-launch');

test('Check for presence of adobe analytics logo', async t => {
  const images = Selector('img');
  const count = await images.count;
  let imageFound = false;

  // Check each image on page for the appropriate src.
  for (let i = 0; i < count; i++) {
    const url = await images.nth(i).getAttribute('src');
    if (url === '/images/homepage/adobe-analytics-logo.png') {
      imageFound = true;
      break;
    }
  }

  assert(imageFound);
})
