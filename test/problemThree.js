import {Selector} from 'testcafe';
const assert = require('assert');

fixture('problemThree').page('https://status.cloud.google.com');

// Script to be injected in page.
// The value of sessionID will be logged in the client console.
const script = "(function() {" +
  "const qtm = document.createElement('script');" +
  "qtm.type = 'text/javascript';" +
  "qtm.async = 1;" +
  "qtm.src = 'https://cdm.quantummetric.com/instrumentation/quantum-qa1.js';" +
  "const d = document.getElementsByTagName('script')[0];" +
  "!window.QuantumMetricAPI && d.parentNode.insertBefore(qtm, d);" +
"})();" +
"window['QuantumMetricOnload'] = function() {" +
  "QuantumMetricAPI.addEventListener('start', function() {" +
    "console.log(QuantumMetricAPI.getSessionID());" +
  "})" +
"}";

test('Inject tag and verify sessionID is defined', async t => {
  // Get message from client console and verify sessionID is defined.
  const sessionMessages = await t.getBrowserConsoleMessages();
  assert(sessionMessages.log[0] != 'undefined'
    && sessionMessages.log[0] != 'null'
    && typeof(sessionMessages.log[0]) === 'string');
}).clientScripts({content: script});
