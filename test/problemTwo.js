import {Selector} from 'testcafe';
const assert = require('assert');

fixture('problemTwo').page('https://status.cloud.google.com');

// Script to be injected in page.
// The value of sessionID will be logged in the client console.
const script = "(function() {" +
  "const qtm = document.createElement('script');" +
  "qtm.type = 'text/javascript';" +
  "qtm.async = 1;" +
  "qtm.src = 'https://cdm.quantummetric.com/instrumentation/quantum-qa1.js';" +
  "const d = document.getElementsByTagName('script')[0];" +
  "!window.QuantumMetricAPI && d.parentNode.insertBefore(qtm, d);" +
"})();" +
"window['QuantumMetricOnload'] = function() {" +
  "console.log(QuantumMetricAPI.getSessionID());" +
"}";

test('Inject tag and verify sessionID is not yet established', async t => {
  // Get message from client console and ensure sessionID is undefined.
  const sessionMessages = await t.getBrowserConsoleMessages();
  assert(sessionMessages.log[0] === 'undefined');
}).clientScripts({content: script});
